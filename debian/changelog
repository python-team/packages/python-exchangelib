python-exchangelib (5.5.0-1) unstable; urgency=low

  * New upstream version 5.5.0
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Thu, 21 Nov 2024 15:43:36 +0000

python-exchangelib (5.4.3-1) unstable; urgency=low

  * New upstream version 5.4.3
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Tue, 22 Oct 2024 20:27:45 +0000

python-exchangelib (5.4.2-1) unstable; urgency=low

  * New upstream version 5.4.2
  * Refresh patches.
  * Bump Standards-Version to 4.7.0.

 -- Michael Fladischer <fladi@debian.org>  Fri, 19 Jul 2024 09:14:31 +0000

python-exchangelib (5.4.0-1) unstable; urgency=low

  * New upstream version 5.4.0
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Sun, 26 May 2024 20:57:12 +0000

python-exchangelib (5.2.0-1) unstable; urgency=low

  * New upstream version 5.2.0
  * Refresh patches.
  * Update year in d/copyright.
  * Build using pybuild-plugin-pyproject.
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Mon, 18 Mar 2024 21:54:42 +0000

python-exchangelib (5.1.0-2) unstable; urgency=medium

  * Team Upload.
  * remove useless, deprecated, build-dep python3-future
  * use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Thu, 04 Jan 2024 14:37:55 +0100

python-exchangelib (5.1.0-1) unstable; urgency=medium

  * New upstream version 5.1.0
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Tue, 12 Sep 2023 11:23:06 +0000

python-exchangelib (5.0.3-1) unstable; urgency=medium

  * New upstream version 5.0.3
  * Bump Standards-Version to 4.6.2.
  * Update lintian-overrides.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Tue, 20 Jun 2023 15:14:06 +0000

python-exchangelib (4.9.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Update d/copyright with new years.

 -- Michael Fladischer <fladi@debian.org>  Thu, 05 Jan 2023 22:34:40 +0000

python-exchangelib (4.8.0-1) unstable; urgency=low

  * New upstream release.
  * Use github tags instead of releases for d/watch.
  * Add linitan overrides for upstream documentation generated with pdoc
    (not packaged for Debian).

 -- Michael Fladischer <fladi@debian.org>  Sun, 09 Oct 2022 19:38:04 +0000

python-exchangelib (4.7.6-1) unstable; urgency=medium

  [ Ileana Dumitrescu ]
  * Team upload.
  * New upstream release.
  * Update exiting patch.
  * Add patch 0002-add-kyiv.patch to fix FTBFS (Closes: #1017188)

  [ Jeroen Ploemen ]
  * Control: bump Standards-Version to 4.6.1 (from 4.6.0; no further
    changes).

 -- Ileana Dumitrescu <ileanadumitrescu95@gmail.com>  Thu, 08 Sep 2022 16:13:56 +0000

python-exchangelib (4.7.2-2) unstable; urgency=low

  * Add python3-tz to Build-Depends, as python3-tzlocal no longer pulls
    it in.

 -- Michael Fladischer <fladi@debian.org>  Thu, 17 Mar 2022 16:17:01 +0000

python-exchangelib (4.7.2-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Mon, 31 Jan 2022 09:56:24 +0000

python-exchangelib (4.6.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.1.
  * Use uscan version 4.
  * Add patch to not depend on tzdata.
  * Update year in d/copyright.
  * Install testfiles using d/pybuild.testfiles.
  * Run tests in build directory instead of source directory.
  * Enable upstream testsuite for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Thu, 02 Dec 2021 21:37:00 +0000

python-exchangelib (3.2.0-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Wed, 10 Jun 2020 19:37:08 +0200

python-exchangelib (3.1.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.5.0.

 -- Michael Fladischer <fladi@debian.org>  Thu, 06 Feb 2020 19:37:49 +0100

python-exchangelib (3.1.0-1) unstable; urgency=low

  * New upstream release.
  * Add debian/gbp.conf.

 -- Michael Fladischer <fladi@debian.org>  Wed, 22 Jan 2020 13:30:44 +0100

python-exchangelib (2.1.1-1) unstable; urgency=low

  * Initial release (Closes: #948171).

 -- Michael Fladischer <fladi@debian.org>  Sat, 04 Jan 2020 21:05:11 +0100
